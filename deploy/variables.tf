variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops-amorel"
}

variable "contact" {
  default = "amorel@ippon.fr"
}

