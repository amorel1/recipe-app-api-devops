variable "region" {
  type    = string
  default = "us-east-1"
}

provider "aws" {
  region  = var.region
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

terraform {
  backend "s3" {
    bucket         = "amorel-recipe-app-api-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "amorel-recipe-app-api-devops-tf-state-lock"
  }
}


